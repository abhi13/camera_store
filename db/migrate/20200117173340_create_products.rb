class CreateProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :products do |t|
      t.string :name
      t.integer :category_id
      t.text :description
      t.decimal :price, precision: 10, scale: 2
      t.integer :make

      t.timestamps
    end
  end
end
