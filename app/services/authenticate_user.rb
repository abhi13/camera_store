class AuthenticateUser
  attr_reader :email, :password
  
  def initialize(email, password)
    @email = email
    @password = password
  end

  # Service entry point
  def call
    user if user
    JsonWebToken.encode(user_id: user.id) if user
  end

  private
  # verify user credentials
  def user
    user = User.find_by(email: email)
    return user if user && user.authenticate(password)
    raise(ExceptionHandler::AuthenticationError, 'Invalid credentials')
  end
end
