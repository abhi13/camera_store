module ExceptionHandler
  extend ActiveSupport::Concern

  # Define custom error subclasses - rescue catches `StandardErrors`
  class AuthenticationError < StandardError; end
  class MissingToken < StandardError; end
  class InvalidToken < StandardError; end

  included do
    around_action :handle_exceptions
    # Define custom handlers
    rescue_from ExceptionHandler::AuthenticationError, with: :unauthorized_request
    rescue_from ExceptionHandler::MissingToken, with: :four_twenty_two
    rescue_from ExceptionHandler::InvalidToken, with: :four_twenty_two
  end

  private

  def handle_exceptions
    begin
      yield
    rescue ActiveRecord::RecordNotFound => e
      @status = 404
    rescue ArgumentError => e
      @status = 400
    rescue ActiveRecord::RecordInvalid => e
      @status = 422
    rescue StandardError => e
      @status = 500
    end
    detail = { detail: e&.message }
    detail.merge!(trace: e&.backtrace) if Rails.env.development?
    # json_response({ success: false, message: e.class.to_s, errors: [detail] }, @status) unless e.class == NilClass
    render json: { success: false, message: e.class.to_s, errors: [detail] }, status: status unless e.class == NilClass
  end

  # JSON response with message; Status code 422 - unprocessable entity
  def four_twenty_two(e)
    render_responses({ errors: e.message }, :unprocessable_entity)
  end

  # JSON response with message; Status code 401 - Unauthorized
  def unauthorized_request(e)
    render_responses({ errors: e.message }, :unauthorized)
  end

  def render_responses(object, status = :ok)
    render json: object, status: status
  end
end
