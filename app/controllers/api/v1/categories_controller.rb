class Api::V1::CategoriesController < ApplicationController
  before_action :set_category, only: %i[show update destroy]

  def index
    categories = Category.all
    render json: categories
  end

  def show
    render json: @category
  end

  def create
    begin
      category = Category.create!(category_params)
      render json: category, status: :created
    rescue => e
      render json: {errors: e.message}, status: :unprocessable_entity
    end
  end

  def update
    if @category.update(category_params)
      render json: @category
    else
      render json: @category.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @category.destroy
      render json: { success: true }
    else
      render json: { success: false }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_category
    @category = Category.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def category_params
    params.require(:category).permit(:id, :name, :type, :model)
  end
end
