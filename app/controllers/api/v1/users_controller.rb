class Api::V1::UsersController < ApplicationController
  before_action :set_user, only: %i[show update destroy view_cart]

  def index
    users = User.all
    render json: users
  end

  def show
    render json: @user
  end

  def create
    begin
      user = User.create!(user_params)
      render json: user, status: :created
    rescue => e
      render json: {errors: e.message}, status: :unprocessable_entity
    end
  end

  def update
    begin
      @user.update!(user_params)
      render json: @user
    rescue => e
      render json: {errors: e.message}, status: :unprocessable_entity
    end
  end

  def destroy
    if @user.destroy
      render json: { success: true }
    else
      render json: { success: false }
    end
  end

  def view_cart
    render json: @user.cart.cart_products
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def user_params
    params.require(:user).permit(:id, :name, :email, :password)
  end
end
