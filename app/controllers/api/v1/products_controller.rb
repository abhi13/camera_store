class Api::V1::ProductsController < ApplicationController
  before_action :set_product, only: %i[show update destroy]

  def index
    if params[:category_id]
      products = Product.includes(:category).where(category_id: params[:category_id])
    else
      products = Product.includes(:category).all
    end
    render json: products || []
  end

  def show
    render json: @product
  end

  def create
    begin
      product = Product.create!(product_params)
      render json: product, status: :created
    rescue => e
      render json: {errors: e.message}, status: :unprocessable_entity
    end
  end

  def update
    if @product.update(product_params)
      render json: @product
    else
      render json: @product.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @product.destroy
      render json: { success: true }
    else
      render json: { success: false }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_product
    @product = Product.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def product_params
    params.require(:product).permit(:id, :name, :category_id, :description, :price, :make)
  end
end
