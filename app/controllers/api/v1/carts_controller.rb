class Api::V1::CartsController < ApplicationController
  before_action :set_cart, only: %i[show add_products remove_products]

  def show
    render json: @cart.cart_products
  end

  def add_products
    cart_product = @cart.add_product(cart_params[:cart_products])
    render json: cart_product
    rescue => e
      render json: e.errors, status: :unprocessable_entity
  end
  
  def remove_products
    cart_product = @cart.remove_product(cart_params[:cart_products])
    render json: cart_product || {message: "Product removed successfully"}
    rescue => e
      render json: e.errors, status: :unprocessable_entity
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_cart
    @cart = Cart.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def cart_params
    params.require(:cart).permit(:id, cart_products: [:product_id, :quantity])
  end
end
