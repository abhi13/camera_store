class Api::V1::Users::SessionsController < ApplicationController
  skip_before_action :authorize_request

  def create
    auth_token = AuthenticateUser.new(params[:email], params[:password]).call
    render_responses(user: auth_token)
  rescue ExceptionHandler::AuthenticationError => e
    render json: { errors: e.message }, status: 422
  end
end
