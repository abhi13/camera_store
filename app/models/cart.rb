class Cart < ApplicationRecord
  belongs_to :user

  has_many :cart_products, dependent: :destroy
  has_many :products, through: :cart_products

  def add_product(product_details)
    cart_product = cart_products.where(product_id: product_details[:product_id]).first
    if cart_product
      new_quantity = cart_product.quantity + 1
      cart_product.update!(quantity: new_quantity)
    else
      cart_product = cart_products.create!(product_id: product_details[:product_id], quantity: 1)
    end
    cart_product
  end

  def remove_product(product_details)
    cart_product = cart_products.find_by(product_id: product_details[:product_id])
    if cart_product
      if cart_product.quantity > 1
        new_quantity = cart_product.quantity - 1
        cart_product.update!(quantity: new_quantity)
      else
        cart_product.destroy
        cart_product = nil
      end
    end
    cart_product
  end
end
