class Category < ApplicationRecord
  self.inheritance_column = nil
  enum type: {mirrorless: 0, full_frame: 1, point_and_shoot: 2}
  has_many :products

  validates_presence_of :type, :name, :model
end
