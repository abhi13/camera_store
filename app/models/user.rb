class User < ApplicationRecord
  has_secure_password

  has_one :cart, dependent: :destroy

  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }, presence: true, uniqueness: true
  validates :password, presence: true

  after_create :create_cart

  def create_cart
    Cart.create!(user: self)
  end
end
