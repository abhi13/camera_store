Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      post 'sign_up', to: 'users#create', as: 'sign_up'
      post 'authenticate', to: 'users/sessions#create', as: 'login'
      resources :users do
        member do
          get :view_cart
        end
      end
      resources :categories, :products
      resources :carts do
        member do
          post :add_products
          post :remove_products
        end
      end
    end
  end
end
